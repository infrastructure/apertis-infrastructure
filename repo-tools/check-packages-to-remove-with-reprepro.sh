#!/bin/bash
#
## Copyright (C) 2017 Andrew Lee (李健秋 <andrew.lee@collabora.co.uk>
##
## This program comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.
##
## This script prints the source packages that have been removed from
## OBS but are still available in the apt repository managed by reprepro
## and can thus be manually removed

set -e

APERTIS_RELEASE=$1

REPO=https://repositories.apertis.org/apertis/
COMPONENTS="target development sdk helper-libs hmi"
SOURCES="source/Sources.gz"
OBSPACKAGES=$(mktemp -t check-packages-to-remove.XXXXXXXXXX)

if [ -z "${APERTIS_RELEASE}" ]
then
    echo "Usage: $0 <Apertis-Suite>"
    echo "  e.g. $0 17.12"
    exit 0
fi

for component in ${COMPONENTS}; do
    osc ls apertis:${APERTIS_RELEASE}:$component
done | sort >> $OBSPACKAGES

for component in ${COMPONENTS}; do
    wget -q ${REPO}/dists/${APERTIS_RELEASE}/${component}/${SOURCES} -O - | \
      zgrep ^Package: | \
      cut -f2 -d" " | \
      sort | \
      comm -1 -3 $OBSPACKAGES - | \
      xargs -r -n 1 -I PKG echo PKG "should be dropped from $APERTIS_RELEASE"
done
