#!/usr/bin/python3

"""Publish Apertis repositories using Aptly

Usage: apertis-publish [options] create
       apertis-publish [options] update [NAME]
       apertis-publish [options] mirror-update
       apertis-publish [options] snap-init [NAME]
       apertis-publish [options] snap-create [NAME]
       apertis-publish [options] snap-update [NAME]
       apertis-publish help

Options:
  -h --help            Show this screen
  -n --no-act          Just print the commands but do nothing
  --release VER        Set release version [default: v2021dev3]
  --dist DIST          Set distribution [default: apertis]
  --arch ARCH          Set architectures [default: amd64 arm64 armhf]
  --components COMPS   Set components [default: hmi target development sdk]
  --repo URL           Set repository URL [default: https://repositories.apertis.org]
  --s3-endpoint NAME   Set S3 endpoint name [default: repository]
  --extra-repos NAMES  Set extra repository names [default: security updates]
  --keyring FILE       Set path to the keyring [default: /etc/apt/trusted.gpg.d/apertis-archive-keyring.gpg]
  -k --sign-key KEY    PGP key to sign published repositories with

Commands:
  create:         Only create an aptly mirror repository. Mirror creation is
                  mostly a one-time operation.
  update:         Update the mirror, create and publish a snapshot to the
                  remote S3 store. This is the most common option most users
                  would use.
  mirror-update:  Only update the mirror to the aptly store. Very useful to
                  run independently if you have a very large repository
  snap-init:      Publish initial snapshot. Can also use a user-provided custom
                  snapshot name.
  snap-create:    Create manual snapshot. Can also use a user-provided custom
                  snapshot name.
  snap-update:    Explicitly update snapshots. Can also use a user-provided
                  snapshot name.

Example: apertis-publish update 20200414

Will run these operations:
* Update the mirrors
* Create snapshots from the mirrors. Will use the string "20200414" as the snapshot identifier
* Publish snapshots created with string "20200414"
"""

import sys
from typing import List

if sys.version_info.minor < 6:
    print("Error: Python 3.6 or later required.\n")
    sys.exit(1)

import time

try:
    import docopt
    import sh
    if sh.__version__ < '1.12':
        print(f"sh version {sh.__version__} found!")
        raise ImportError
except ImportError:
    print("Error: Required Python packages not found.")
    print("Install docopt and sh (>= 1.12), e.g. apt install python3-docopt python3-sh\n")
    sys.exit(1)

from docopt import docopt

def setup_aptly(no_act: bool):
    global aptly
    if not no_act:
        try:
            from sh import aptly as unbaked_aptly
        except ImportError:
            print("Error: Aptly not found in the current PATH.")
            print("Install aptly, e.g. apt install aptly\n")
            sys.exit(1)
    else:
        from sh import echo
        unbaked_aptly = echo.bake('#').aptly
    aptly = unbaked_aptly.bake(_fg=True)

def filter_arch(comp: str, arch: List[str]) -> str:
    if comp == 'sdk':
        return ['amd64']
    else:
        return arch

def format_timestamp(timestamp: int):
    return time.strftime("%Y%m%dT%H%M%SZ", time.gmtime(timestamp))

def mirror_create(dist, release, arch, components, keyring, repo, **args):
    for comp in components:
        comp_arch = filter_arch(comp, arch)
        print(f"Creating aptly mirrors for {dist}:{release}:{comp} for architectures: {' '.join(comp_arch)}")
        aptly.mirror.create(
            "-force-architectures",
           f"-architectures={','.join(comp_arch)}",
           f"-keyring={keyring}",
           f"{dist}-{release}/{comp}",
           f"{repo}/{dist}",
           f"{release}",
           f"{comp}",
        )

def mirror_update(dist, release, arch, components, keyring, **args):
    for comp in components:
        print(f"Updating aptly mirrors for {dist}:{release}:{comp}")
        aptly.mirror.update(
           f"-keyring={keyring}",
           f"{dist}-{release}/{comp}",
        )

def snap_create(dist, release, arch, components, name, **args):
    for comp in components:
        print(f"Creating snapshot {dist}-{release}/{comp}/{name}")
        aptly.snapshot.create(
           f"{dist}-{release}/{comp}/{name}",
            "from",
            "mirror",
           f"{dist}-{release}/{comp}",
        )

def snap_update(dist, release, arch, components, name, timestamp, s3_endpoint, sign_key, **args):
    s3 = f"s3:{s3_endpoint}:"
    snapshots = [f"{dist}-{release}/{comp}/{name}" for comp in components]
    snapshot_suffix = f"@{format_timestamp(timestamp)}"
    print(f"Publishing snapshots {snapshots} to {s3} as {release}{snapshot_suffix}")
    aptly.publish.snapshot(
       f"-gpg-key={sign_key}" if sign_key else "-skip-signing",
        "-batch",
       f"-distribution={release}{snapshot_suffix}",
       f"-component={','.join(components)}",
       *snapshots,
       f"{s3}"
    )
    for comp in components:
        print(f"Updating {release} ({comp}) at {s3} to snapshot {dist}-{release}/{comp}/{name}")
        aptly.snapshot.switch(
            "-skip-signing",
           f"-component={comp}",
           f"{release}",
           f"{s3}"
           f"{dist}-{release}/{comp}/{name}",
        )

def snap_init(dist, release, arch, components, name, timestamp, s3_endpoint, keyring, sign_key, **args):
    mirror_update(dist, release, arch, components, keyring)
    snap_create(dist, release, arch, components, name)

    s3 = f"s3:{s3_endpoint}:"
    snapshots = [f"{dist}-{release}/{comp}/{name}" for comp in components]
    snapshot_suffix = f"@{format_timestamp(timestamp)}"
    print(f"Publishing initial snapshots {snapshots} to {s3} as {release}{snapshot_suffix}")
    aptly.publish.snapshot(
       f"-gpg-key={sign_key}" if sign_key else "-skip-signing",
        "-batch",
       f"-distribution={release}{snapshot_suffix}",
       f"-component={','.join(components)}",
       *snapshots,
       f"{s3}"
    )
    print(f"Publishing initial snapshots {snapshots} to {s3} as {release}")
    aptly.publish.snapshot(
       f"-gpg-key={sign_key}" if sign_key else "-skip-signing",
        "-batch",
       f"-component={','.join(components)}",
       *snapshots,
       f"{s3}"
    )

def create(**args):
    mirror_create(**args)

def update(**args):
    mirror_update(**args)
    snap_create(**args)
    snap_update(**args)

def help(**args):
    print(__doc__.strip("\n"))

if __name__ == '__main__':
    arguments = docopt(__doc__)
    params = {k[2:].replace('-', '_'): v for k, v in arguments.items() if k.startswith('--')}
    params.update({k.lower().replace('-', '_'): v for k, v in arguments.items() if k.isupper()})
    params['arch'] = params['arch'].split()
    params['components'] = params['components'].split()
    params['timestamp'] = int(time.time())
    params['name'] = params['name'] or str(params['timestamp'])

    commands = [x for x in arguments.keys() if not x.startswith('--') and not x.isupper()]
    active_commands = [x.replace('-', '_') for x in commands if arguments[x]]
    if active_commands:
        active_command, *_ = active_commands

    setup_aptly(params['no_act'])
    locals()[active_command](**params)
