###################################################################################
# LAVA CI callback webservice
#
# Configuration module
#
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import yaml
from collections import ChainMap

defaults = {
    'url': None,
    'token': None,
    'instance': None,
    'projects': ['infrastructure/apertis-image-recipes'],
    'ref': None,
    'status': None,
    'log-file': None,
    'debug': False,
}

class Config(object):
    storage = ChainMap({}, defaults)

    def load_config(self, args: argparse.Namespace):
        self.merge_config(vars(args))

    def merge_config(self, args: dict):
        if args['config']:
            with open(args['config'], encoding='UTF-8') as conf_data:
                config = yaml.safe_load(conf_data)
        else:
            config = {}
        arguments = {k.replace('_', '-'): v for k, v in args.items() if v}
        self.storage = ChainMap(arguments, config, defaults)

    def __repr__(self):
        return repr(dict(self.storage))

    def __len__(self):
        return self.storage.__len__()

    def __getitem__(self, key: str):
        return self.storage.__getitem__(key)

    def __iter__(self):
        return self.storage.__iter__()

    def get(self, k, d=None):
        return self.storage[k] if k in self.storage else d

config = Config()
