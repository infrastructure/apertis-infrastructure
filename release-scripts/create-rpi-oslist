#!/bin/sh

set -e

base_dir=/srv/images/public/release
rpi_dir=/srv/images/public/rpi
entry_template='{
  "name": "",
  "description": "",
  "url": "",
  "icon": "https://images.apertis.org/rpi/apertis-logo.png",
  "extract_size": 0,
  "extract_sha256": "",
  "image_download_size": 0,
  "image_download_sha256": "",
  "release_date": "",
  "website": "",
  "init_format": "",
  "devices": 0
}'
jq_command_string='. | .name=$name
                     | .description=$description
                     | .url=$url
                     | .extract_size=$extract_size
                     | .extract_sha256=$extract_sha256
                     | .image_download_size=$image_download_size
                     | .image_download_sha256=$image_download_sha256
                     | .release_date=$release_date
                     | .website=$website
                     | .init_format=$init_format
                     | .devices=$devices'

generate_oslist_entry() {
    image_dir="${base_dir}/${distro}/${release}/arm64/${type}"
    image_url="https://images.apertis.org/release/${distro}/${release}/arm64/${type}"

    if [ ! -d ${image_dir} ] ; then
        echo "Folder ${image_dir} is not valid"
        return
    fi

    if [ "${variant}" = "ostree" ]; then
        image_name="apertis_ostree_${distro}-${type}-arm64-rpi64_${release}.img"
        shortdesc="OSTree "
        longdesc="OSTree-based "
    else
        image_name="apertis_${distro}-${type}-arm64-rpi64_${release}.img"
        shortdesc=""
        longdesc=""
    fi
    image_file="${image_dir}/${image_name}"

    if [ "${type}" = "fixedfunction" ] || [ "${type}" = "minimal" ]; then
        typedesc="headless"
        typelink="fixedfunction"
    elif [ "${type}" = "hmi" ] || [ "${type}" = "target" ]; then
        typedesc="graphical"
        typelink="hmi"
    else
        typedesc="${type}"
        typelink="${type}"
    fi

    echo "${entry_template}" | jq \
        --arg name "Apertis $distro ${shortdesc}${typedesc} image" \
        --arg description "Apertis $distro ${longdesc}${typedesc} image for Raspberry Pi 3/4" \
        --arg url "${image_url}/${image_name}.gz" \
        --argjson extract_size $(grep ImageSize ${image_file}.bmap | sed 's/.* \([[:digit:]]*\) .*/\1/') \
        --arg extract_sha256 "$(zcat ${image_file}.gz | sha256sum | cut -f 1 -d ' ')" \
        --argjson image_download_size $(stat -c %s ${image_file}.gz) \
        --arg image_download_sha256 "$(cat ${image_file}.gz.sha256 | cut -f 1 -d ' ')" \
        --arg release_date "$(stat -c %w ${image_file}.gz | cut -f 1 -d ' ')" \
        --arg website "https://www.apertis.org/policies/images/#${typelink}" \
        --arg init_format "systemd" \
        --argjson devices "[\"pi4-64bit\",\"pi3-64bit\"]" \
        "${jq_command_string}" \
        > ${tmpdir}/oslist-entry-${type}-${variant}.json
}

if [ "$#" -lt 1 ]
then
    echo "Usage: $0 <release-name>" >&2
    exit 1
fi

release=$1
distro=${release%.*}

if echo $release | grep -qE "(dev|pre)"; then
    list="dev"
    lastdistro=$(ls ${base_dir}/ | grep -E "(dev|pre)" | sort | tail -1)
else
    list="stable"
    lastdistro=$(ls ${base_dir}/ | grep -Ev "(dev|pre)" | sort | tail -1)
fi

if [ "$distro" != "$lastdistro" ]; then
    echo "Not creating oslist for an old release"
    exit 0
fi

tmpdir=$(mktemp -d /tmp/apertis-oslist.XXXXXXXX)

# Generate entries for each available image
for type in "minimal" "target" "fixedfunction" "hmi"; do
    for variant in "apt" "ostree"; do
        generate_oslist_entry
    done
done

# Create the full list by including all individual entries
echo '{"os_list":[]}' > ${tmpdir}/apertis-oslist-${list}.json
idx=0
for file in ${tmpdir}/oslist-entry-*.json; do
    entry="$(cat $file)"
    cat ${tmpdir}/apertis-oslist-${list}.json | jq \
        --argjson entry "${entry}" \
        ". | .os_list[${idx}]=\$entry" \
        > ${tmpdir}/tmp.json
    mv ${tmpdir}/tmp.json ${tmpdir}/apertis-oslist-${list}.json
    idx=$(($idx+1))
done

# Deploy the full list
mkdir -p ${rpi_dir}
mv ${tmpdir}/apertis-oslist-${list}.json ${rpi_dir}
rm -rf ${tmpdir}
