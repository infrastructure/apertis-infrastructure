#!/bin/bash

api(){
    set -e

    if [ -z "$GITLAB_TOKEN" ]
    then
        . ~/.gitlab-apertis-org-token
    fi

    if [ -z "$CI_SERVER_HOST" ]
    then
        echo "CI_SERVER_HOST not specified, using default" >&2
        CI_SERVER_HOST=gitlab.apertis.org
    fi

    api_base_url=https://$CI_SERVER_HOST/api/v4
    api_endpoint=$1

    if echo "$api_endpoint" | grep -q '?'
    then
      items="&per_page=100"
    else
      items="?per_page=100"
    fi
    shift

    if [ "$DEBUG" = "1" ]; then
        set -x
    fi

    curl -s -H "PRIVATE-TOKEN: $GITLAB_TOKEN" --request ${request:-GET} $api_base_url$api_endpoint$items "$@"

    if [ "$DEBUG" = "1" ]; then
        set +x
    fi
}

get-id(){
    echo $1 | sed -e s,/,%2F,g -e s,+,-,g
}

create-merge-request(){
    repoid=$(get-id $1)
    
    request=POST api /projects/$repoid/merge_requests -d source_branch=$2 -d target_branch=$3 -d title="Merge changes from $2 into $3"
}

create-repo-branch(){
    repoid=$(get-id $1)

    request=POST api /projects/$repoid/repository/branches -d branch=$2 -d ref=$3
}

create-repo-ci-schedule(){
    repoid=$(get-id $1)
    branch_ref=$(get-id $2)

    request=POST api /projects/$repoid/pipeline_schedules -d description="create CI Schedule" -d ref="$branch_ref" -d cron="0 3 * * *"
}

create-repo-trigger(){
    repoid=$(get-id $1)

    request=POST api /projects/$repoid/triggers -d description="create trigger"
}

delete-repo-branch(){
    repoid=$(get-id $1)
    branchid=$(get-id $2)

    request=DELETE api /projects/$repoid/repository/branches/$branchid
}

get-groups-repos(){
    groupid=$(get-id $1)

    api "/groups/$groupid/projects?with_shared=false&archived=false" | jq -r .[].path
}

get-repo-branch(){
    repoid=$(get-id $1)
    branchid=$(get-id $2)

    api /projects/$repoid/repository/branches/$branchid | jq -r '.commit.short_id'
}

get-repo-branches(){
    repoid=$(get-id $1)

    api /projects/$repoid/repository/branches | jq -r '.[] | [.name, .commit.short_id] | @tsv'
}

get-repo-head(){
    repoid=$(get-id $1)

    api /projects/$repoid | jq -r '.default_branch'
}

get-repo-path(){
    repoid=$(get-id pkg/$1)
    path=$(api /projects/$repoid | jq -r '.path_with_namespace')

    if [ "$path" != null ]; then
        echo $path
    fi
}

get-repo-tags(){
    repoid=$(get-id $1)

    api /projects/$repoid/repository/tags | jq -r '.[] | [.name, .commit.short_id] | @tsv'
}

get-repo-triggers(){
    repoid=$(get-id $1)

    api /projects/$repoid/triggers | jq -r '.[] | [.token] | @tsv'
}

merge-mr(){
    # Merge a MR from a repo

    set -e

    usage () {
        echo "Usage: merge-mr <package> <mr-id>" >&2
        echo "" >&2
        echo "Example: merge-mr libfoo 1" >&2
    }

    if [ "$#" -ne 2 ]; then
        usage
        return 1
    fi

    repoid=$(get-id $1)
    mr_id=$2

    request=PUT api /projects/$repoid/merge_requests/$mr_id/merge -d merge_commit_message="Auto merge changes for this MR, if the pipeline succeeds" -d merge_when_pipeline_succeeds="true" -d should_remove_source_branch="true"
}

trigger-pipeline(){
    # Trigger pipeline on selected package

    set -e

    usage () {
        echo "Usage: trigger-pipeline <package> <pipeline>" >&2
        echo "" >&2
        echo "Example: trigger-pipeline libfoo debian/sid" >&2
    }

    if [ "$#" -ne 2 ]; then
        usage
        return 1
    fi

    pkg_path=$(get-repo-path $1)
    repoid=$(get-id $pkg_path)
    ref=$2

    request=POST api /projects/$repoid/pipeline?ref=$ref
}

