#!/bin/sh

DEFAULTCONFIG="
Repotype: debian
type: dsc
release: b${NEXT_RELEASE}.b<B_CNT>
buildengine: debootstrap
"

OLDCONFIG=$(osc meta prjconf $OSNAME:$RELEASE:$c)
CONFIG=$(echo "$OLDCONFIG" | sed "s/$RELEASE/$NEXT_RELEASE/g")

if [ -z "$CONFIG" ]
then
  CONFIG=$DEFAULTCONFIG
fi

echo "$CONFIG"
echo "$CONFIG" | $run osc meta prjconf $OSNAME:$NEXT_RELEASE:$c -F -
