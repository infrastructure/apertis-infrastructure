help() {
    echo Usage:
    printf "  %s " $0
    echo $(sed -n '2,/\. .*common.sh$/ {/=\$/p}' $0 | cut -d= -f1 | tr a-z A-Z)
    echo
    sed -n '2,/\. .*common.sh$/ {/^# /{s/^# //g;p}}' $0
}

# Show help if --help is passed or parameters needed and none given
if [ "$1" = --help ] || [ $# -lt 1 -a $(sed -n '2,/\. .*common.sh$/ {/=\$/p}' $0 | wc -l) -ne 0 ]
then
    help >&2
    exit 1
fi

old_next_release() {
	major=${1%%.*}
	minor=${1##*.}

	minor=$((${minor#0} / 4 + 1))

	if [ $minor -gt 3 ]
	then
		minor=$(($minor - 4))
		major=$(($major + 1))
	fi

	printf v20%.2ddev%d $major $minor
}

major() {
    y=${1%%dev*}
    echo ${y#v20}
}

quarter() {
    echo ${1##v20??}
}

new_next_release() {
    major=${1%%dev*}
    minor=${1##*dev}

    major=${major#v}

    minor=$(($minor + 1))

    if [ $minor -gt 3 ]
    then
		minor=$(($minor - 4))
		major=$(($major + 1))
    fi

    printf v%.4ddev%d $major $minor
}

next_release() {
    case "$1" in
        v*dev*)
            new_next_release "$1"
            ;;
        *.*)
            old_next_release "$1"
            ;;
        *)
            echo E: Wrong distro version format
            exit 1
            ;;
    esac
}
