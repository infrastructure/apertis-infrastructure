#!/usr/bin/python3
# SPDX-License-Identifier: MIT

import os
import git
import gitlab
import gitlab.v4.objects
import time
import argparse
import logging
import json
import base64

class MergeData:
    def __init__(self, mr_url, component, trivial_merge, failed_merge, changes_url, pipeline_url, pipeline_status, failed_jobs, project_id, merge_request_iid):
        self.mr_url = mr_url
        self.component = component
        self.trivial_merge = trivial_merge
        self.failed_merge = failed_merge
        self.changes_url = changes_url
        self.pipeline_url = pipeline_url
        self.pipeline_status = pipeline_status
        self.failed_jobs = failed_jobs
        self.project_id = project_id
        self.merge_request_iid = merge_request_iid

    def __str__(self):
        if self.trivial_merge:
            if self.pipeline_status == "success":
                prefix = "🚀 Trivial"
            else:
                prefix = "😿 Trivial but failed"
        elif self.failed_merge:
            prefix = "⚠️  Automated merge failed:"
        else:
            prefix = "🚨 To check:"

        failed = ""
        if self.failed_jobs:
            failed = f" (failed: {self.failed_jobs})"

        return f"{prefix}: {self.mr_url} - {self.component} - pipeline: {self.pipeline_status}{failed}, {self.pipeline_url}  - changes: {self.changes_url}"

class Analyser:
    def __init__(self, group):
        self.gl = None
        self.group = group

    def connect(self, gitlab_instance, gitlab_server_url, gitlab_api_token):
        if gitlab_server_url:
            logging.debug(f'Connecting to the "{gitlab_server_url}" instance')
            self.gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_api_token)
        else:
            logging.debug(f'Connecting to the "{gitlab_instance}" configured instance')
            self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()

    def analyse_mr(self, mr):
        logging.debug(f"Analyzing {mr.web_url}")

        project = self.gl.projects.get(mr.project_id, lazy=True)
        pmr = project.mergerequests.get(mr.iid, access_raw_diffs=False, lazy=False)
        logging.debug(f"From {pmr.source_branch} to {pmr.target_branch}")

        component = None
        try:
            items = project.repository_tree(path='debian/apertis/', ref=pmr.target_branch);
            for  i in items:
                if i['name'] == "component":
                    info = project.repository_blob(i['id'])
                    component = base64.b64decode(info['content']).decode().rstrip()
        except:
            pass

        cmp = project.repository_compare(args.debian_branch, pmr.source_branch)
        logging.debug(f"Changes url: {cmp['web_url']}")
        trivial_merge = True
        for d in cmp['diffs']:
            old = d['old_path']
            new = d['new_path']
            if old == new:
                logging.debug(f"Changes to file: {new}")
                if new != "debian/changelog" and not new.startswith("debian/apertis/"):
                    trivial_merge = False
            else:
                logging.debug(f"File move: {old} -> {new}")
                trivial_merge = False

        failed_merge = False
        if len(cmp['diffs']) == 0:
            trivial_merge = False
            failed_merge = True

        p = pmr.pipelines.list()[0]
        logging.debug(f"Pipeline: {p.web_url} -> {p.status}")
        failed_jobs = []
        if p.status != "success":
            p = project.pipelines.get(p.id)
            failed_jobs = list(map(lambda j: j.name, filter(lambda j: j.status == "failed", p.jobs.list())))
            logging.debug(f"Pipeline jobs that failed: {failed_jobs}")
        logging.debug(f"Found a trivial merge: {trivial_merge}")

        m = MergeData(mr.web_url, component, trivial_merge, failed_merge, cmp['web_url'], p.web_url, p.status, failed_jobs, mr.project_id, mr.iid)
        logging.info(m)
        return m

    def collect_data(self, target, debian, user, max_merges = None):
        merges = []
        group = self.gl.groups.get(f"{self.group}", lazy=True);
        for mr in group.mergerequests.list(state = "opened", iterator=True, lazy = True):
            if mr.target_branch != target:
                continue
            if mr.author["username"] != user:
                continue
            if max_merges != None:
                if max_merges <= 0:
                    break
                max_merges -= 1

            data = self.analyse_mr(mr)
            merges.append(data)
        return merges

    def merge(self, data):
        project = self.gl.projects.get(data.project_id, lazy=True)
        pmr = project.mergerequests.get(data.merge_request_iid, access_raw_diffs=False, lazy=False)
        logging.info(f"Enabling merge for {pmr.web_url}")
        try:
            pmr.merge(merge_when_pipeline_succeeds=True)
        except:
            logging.warning(f"Unable to merge {pmr.web_url}")

    def retry_pipeline(self, data):
        project = self.gl.projects.get(data.project_id, lazy=True)
        pmr = project.mergerequests.get(data.merge_request_iid, access_raw_diffs=False, lazy=False)
        p = pmr.pipelines.list()[0]
        failed_jobs = []
        if p.status != "success":
            logging.info(f"Retrying pipeline for: {data.pipeline_url}")
            p = project.pipelines.get(p.id)
            p.retry()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Analyse merges as part of a rebase effort"
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument("--gitlab-api-token", type=str, help="the GitLab API token")
    parser.add_argument("--gitlab-server-url", type=str, help="the GitLab instance URL")
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )

    parser.add_argument('--max-merges',
                         type=int,
                         default = None)
    parser.add_argument('--automerge-trivial',
                         action='store_true')
    parser.add_argument('--debian-branch',
                        default = 'debian/trixie',
                        help = 'Debian upstream branch to use')
    parser.add_argument('--apertis-target-branch',
                        default = 'apertis/v2026dev2',
                        help = 'Debian upstream branch to use')
    parser.add_argument('--filter-user',
                        default = "not_a_robot",
                        help = "Only consider MR from this user")
    parser.add_argument('--json',
                         action='store_true',
                         help = "Be quiet and results as json")
    parser.add_argument('--retry-obs',
                         action='store_true',
                         help = "Retry pipelines with OBS fail")
    parser.add_argument('--group',
                        default = 'pkg',
                        help = 'Gitlab group were package are stored')

    args = parser.parse_args()
    if args.json:
        args.loglevel = logging.ERROR

    logging.basicConfig(format='%(asctime)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=args.loglevel or logging.INFO)

    a = Analyser(args.group)
    a.connect(args.gitlab_instance, args.gitlab_server_url, args.gitlab_api_token)
    merges = a.collect_data(args.apertis_target_branch, args.debian_branch, args.filter_user, args.max_merges)

    logging.info(f"Total merges analyzed: {len(merges)}")
    logging.info(f"Total trivial merges with happy pipelines: {len(list(filter(lambda m: m.trivial_merge and m.pipeline_status == 'success', merges)))}")
    logging.info(f"Total trivial merges with unhappy pipelines: {len(list(filter(lambda m: m.trivial_merge and m.pipeline_status != 'success', merges)))}")
    logging.info(f"Automated merge failures: {len(list(filter(lambda m: m.failed_merge, merges)))}")
    logging.info(f"Non-trivial merges: {len(list(filter(lambda m: not m.trivial_merge and not m.failed_merge, merges)))}")

    if args.json:
        print(json.dumps(merges, default = lambda x: x.__dict__))

    if args.automerge_trivial:
        for mr in filter(lambda m: m.trivial_merge and m.pipeline_status == 'success', merges):
            a.merge(mr)

    if args.retry_obs:
        for mr in filter(lambda m: m.pipeline_status == 'failed' and len(m.failed_jobs) > 0 and m.failed_jobs[0].startswith(('obs','upload')), merges):
            a.retry_pipeline(mr)
