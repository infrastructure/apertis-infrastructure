#!/usr/bin/python3
# SPDX-License-Identifier: MIT

import os
import gitlab
import gitlab.v4.objects
import time
import argparse
import logging

RENAMED = {
    "dbus-c++": "dbus-cxx",
    "gtk+2.0": "gtk-2.0",
    "gtk+3.0": "gtk-3.0",
    "libsigc++-2.0": "libsigcxx-2.0",
    "libxml++2.6": "libxmlxx2.6",
    "libpcre++": "libpcrexx",
}

class Analyser:
    def __init__(self, group):
        self.gl = None
        self.group = group

    def connect(self, gitlab_instance, gitlab_server_url, gitlab_api_token):
        if gitlab_server_url:
            logging.debug(f'Connecting to the "{gitlab_server_url}" instance')
            self.gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_api_token)
        else:
            logging.debug(f'Connecting to the "{gitlab_instance}" configured instance')
            self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()

    def analyse_diff(self, project, target, debian):
        cmp = project.repository_compare(debian, target)
        logging.debug(f"Changes url: {cmp['web_url']}")
        trivial_merge = True
        for d in cmp['diffs']:
            old = d['old_path']
            new = d['new_path']
            if old == new:
                logging.debug(f"Changes to file: {new}")
                if new != "debian/changelog" and not new.startswith("debian/apertis/"):
                    trivial_merge = False
            else:
                logging.debug(f"File move: {old} -> {new}")
                trivial_merge = False
        return (trivial_merge, cmp['web_url'])

    def analyse_component(self, project, target):
        component = project.files.raw(file_path='debian/apertis/component', ref=target).decode('ascii').strip()

        return component

    def analyse_package(self, package, target, debian, trigger_trivial = False, label = None):
        logging.debug(f"Analysing {package}")
        package = RENAMED.get(package, package)
        logging.debug(f"Analysing {package}")

        try:
            project = self.gl.projects.get(f"{self.group}/{package}")
        except:
            logging.info(f"{package} has no project on gitlab")
            return

        # Check if the package has an open MR against the target
        for mr in project.mergerequests.list(iterator=True, lazy=True):
            if mr.target_branch == target and mr.state == 'opened':
                logging.info(f"{project.web_url} has an open MR - {mr.web_url}")
                if label:
                    if label == "!":
                        mr.labels = []
                    elif label.startswith("!") and label[1:] in mr.labels:
                        mr.labels.remove(label[1:])
                    else:
                        mr.labels.append(label)
                    mr.save()
                return

        try:
            target_branch = project.branches.get(target)
        except:
            logging.info(f"{project.web_url} is missing an {target} branch")
            return

        try:
            debian_branch = project.branches.get(debian)
        except:
            logging.info(f"{project.web_url} is missing an {debian} branch")
            return

        component = self.analyse_component(project, target)

        # Check if debian as already merged into apertis branch
        target_id = target_branch.commit['id']
        debian_id = debian_branch.commit['id']
        mb = project.repository_merge_base([target_id, debian_id])
        if mb['id'] == debian_id:
            pipeline = None
            for p in project.pipelines.list(iterator=True, lazy=True):
                if p.ref == target:
                    p = project.pipelines.get(p.id)
                    if p.status == "success":
                        logging.info(f"{project.web_url} already merged and happy pipeline, component: {component}")
                    else:
                        (trivial, changes) = self.analyse_diff(project, target, debian);
                        logging.info(f"{project.web_url} already merged but unhappy: {p.web_url} - {p.status} - trivial: {trivial} changes: {changes}, component: {component}")
                        if p.status == 'skipped' and trivial and trigger_trivial:
                            project.pipelines.create({'ref': target})
                            logging.info(f"{project.web_url} pipeline created!!")

                    return
            logging.info(f"{project.web_url} is already merged, but no pipeline, component: {component}")
            return

        # Check if the last pipeline for the debian branch succeeded
        pipeline = None
        for p in project.pipelines.list(iterator=True, lazy=True):
            if p.ref == debian:
                pipeline = p

        if pipeline == None:
            logging.info(f"{project.web_url} is a missing pipeline for {debian} branch ")
            return

        logging.info(f"{project.web_url} is missing an MR")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Analyse merges as part of a rebase effort"
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument("--gitlab-api-token", type=str, help="the GitLab API token")
    parser.add_argument("--gitlab-server-url", type=str, help="the GitLab instance URL")
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )

    parser.add_argument('--max-merges',
                         type=int,
                         default = None)
    parser.add_argument('--trigger-trivial',
                         action='store_true',
                         help = 'trigger trivial pipelines')
    parser.add_argument('--debian-branch',
                        default = 'debian/trixie',
                        help = 'Debian upstream branch to use')
    parser.add_argument('--apertis-target-branch',
                        default = 'apertis/v2026dev2',
                        help = 'Debian upstream branch to use')
    parser.add_argument('missing',
                         help = "Be quiet and results as json")
    parser.add_argument('--group',
                        default = 'pkg',
                        help = 'Gitlab group were package are stored')
    parser.add_argument('--label',
                        default = '',
                        help = 'Apply a label to the MR if present')

    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=args.loglevel or logging.INFO)

    a = Analyser(args.group)
    a.connect(args.gitlab_instance, args.gitlab_server_url, args.gitlab_api_token)
    missing = open(args.missing);
    for l in missing.readlines():
        package = l.rstrip();
        a.analyse_package(package, args.apertis_target_branch, args.debian_branch, args.trigger_trivial, args.label)
