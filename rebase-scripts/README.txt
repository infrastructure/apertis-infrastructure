#####
# fork-in-pkg-rebase-trixie

This tool fork a package from https://gitlab.apertis.org/pkg/ into a new
namespace https://gitlab.apertis.org/pkg-rebase-trixie/ allowing us to simulate
the rebase in isolated mode. Here are the steps performed:
1. Fork pkg into pkg-rebase-trixie
2. Apply rulez.yaml from infrastructure/apertis-infrastructure:wip/daissi/rulez-testing-rebase
   (See https://gitlab.apertis.org/infrastructure/apertis-infrastructure/-/merge_requests/210)
  2.1 Change CI to ci-package-builder.yml@infrastructure/ci-package-builder:wip/daissi/add-trixie
      This contains changes from https://gitlab.apertis.org/infrastructure/ci-package-builder/-/merge_requests/339
  2.2 Change default branch to apertis/v2026dev0 (OBS repo is based on dev0, not dev1)
4. Branch upstream/trixie from upstream/bookworm
5. Branch debian/trixie from debian/bookworm
6. Trigger pipeline on debian/trixie

> ./fork-in-pkg-rebase-trixie -p pipewire -t MY-GITLAB-TOKEN

#####
# rebase-from-germinate

This tool runs fork-in-pkg-rebase-trixie on each package of a list generated
by germinate.

See https://gitlab.apertis.org/infrastructure/germinate/-/merge_requests/23
how to generate a list of packages. For instance, once germinate has generated
target-keep.txt, rebase-from-germinate can be used as below:

> ./fork-in-pkg-rebase-trixie -l target-keep.txt -t MY-GITLAB-TOKEN

#####
# MY-GITLAB-TOKEN is a gitlab token allowing to fork projects, to change project
# settings and to trigger new pipelines.
