#!/usr/bin/python3

import argparse
import json
import gitlab
import os
import requests
import subprocess
import sys
import yaml

SHOW_ALL = 0
SHOW_INCOMPLETE = 1
SHOW_FAIL = 2
SHOW_PASS = 3

RETRY_NONE = 0
RETRY_JOB = 1
RETRY_LQA = 2

VERBOSE_NO = 0
VERBOSE_LOW = 1
VERBOSE_NORMAL = 2

JOB_DATA_TRACE = 0
JOB_DATA_ARTIFACT = 1

config = None

def read_config():
    global config
    script_folder = os.path.dirname(__file__)
    with open(os.path.join(script_folder, 'config.yaml')) as y:
        config = yaml.load(y, Loader=yaml.FullLoader)
    return config

def get_buildids(release):
    buildids = []

    url = f'{config["image_url"]}/{release}'
    r = requests.get(url)

    for l in r.iter_lines():
        l = l.decode('ascii').strip()
        if l.find('href') == -1:
            continue
        pos1 = l.find('"')
        pos2 = l.find('"', pos1+1)
        l = l[pos1+1:pos2]
        if l.find('latest') != -1:
            continue
        l = l[:-1]
        buildids.append(l)

    return buildids

def get_latest_orchestrator_buildid(release):

    url = f'{config["image_url"]}/{release}/latest-scheduled-build.txt'
    r = requests.get(url)

    buildid = r.content.decode('ascii').strip()

    return buildid

def get_latest_build(release):
    if release == 'v2023':
        return get_buildids(release)[-1]
    else:
        return get_latest_orchestrator_buildid(release)

def get_build(release, buildid):
    if buildid == 'latest':
        return get_latest_build(release)
    else:
        return  buildid

def get_tests(release, buildid, deployment, verbose):
    print(f'Getting tests for {release} deployment {deployment}')

    url = f'{config["qa_url"]}/report/{release}/{buildid}/{deployment}'
    tests = {}
    r = requests.get(url)
    hrefs = []
    for l in r.iter_lines():
        l = l.decode('ascii').strip()
        if l.find('href') != -1 and l.find('results') != -1:
            hrefs.append(l)
            for str_item in config['string_remove']:
                l = l.replace(str_item, '')
            for str_item in config['string_space']:
                l = l.replace(str_item, ' ')
            test = l.split(' ')
            if test[1] == 'INCOMPLETE':
                test[1] = '*'
                test.append('INCOMPLETE')
            tests[test[0]] = test

    return tests

def filter_tests(tests, filter = SHOW_ALL):
    test_result = None
    if filter == SHOW_INCOMPLETE:
        test_result = 'INCOMPLETE'
    elif filter == SHOW_FAIL:
        test_result = 'FAIL'
    elif filter == SHOW_PASS:
        test_result = 'PASS'

    if test_result:
        tests = {k: v for k, v in tests.items() if v[2] == test_result}

    return tests

def print_tests(tests, verbose):
    for k,v in tests.items():
        if verbose == VERBOSE_LOW:
            print(k)
        elif verbose > VERBOSE_LOW:
            print(k, v)

def retry_tests(tests):
    for k, v in tests.items():
        print(f'Retrying LAVA job {k}')
        os.system(f'lqa resubmit {k}')

def find_pipeline(pipelines, buildid):
    pipeline = None
    for p in pipelines:
        for v in p.variables.list():
            if v.key != 'BUILD_ID':
                continue
            if v.value == buildid:
                pipeline = p
                break
    return pipeline

def get_job_trace(job):
    submitted = {}
    try:
        trace = job.trace()
        trace = trace.decode('utf-8').strip()
        job_data = trace.split('\n')[:2]
        test = job_data[1].replace(config['scheduled_string'], '')
    except Exception as ex:
        print(f'Error retrieving job {job.id} {str(ex)}')
        return submitted
    submitted[test] = [job.id, job.name]
    return submitted

def get_job_artifacts(job):
    submitted = {}

    try:
        os.system('rm -f tmp/*')
        zipfn = "tmp/artifacts.zip"
        with open(zipfn, "wb") as f:
            job.artifacts(streamed=True, action=f.write)
        out = subprocess.run(["unzip", "-d", "tmp", "-o", zipfn], capture_output=True)
        os.unlink(zipfn)
        for f in os.listdir('tmp'):
            if f.find('_log.yaml') == -1:
                continue
            test = f.replace('_log.yaml', '')
            submitted[test] = [job.id, job.name]
        os.system('mv tmp/* artifacts')
    except:
        print(f'Job {job.id} {job.name} has issues')

    return submitted

def get_submitted(release, buildid, job_data):
    ref = 'apertis/' + release

    print(f'Getting submitted tests for {release} buildid {buildid}')

    gl = gitlab.Gitlab.from_config()
    gl.auth()
    project = gl.projects.get(config['gitlab_image_project'])
    pipelines = project.pipelines.list(get_all=False, ref=ref)
    pipeline = find_pipeline(pipelines, buildid)
    if not pipeline:
        print('Pipeline not found')
        return None

    print(f'Retrieving LAVA jobs for {pipeline.id}')

    if job_data == JOB_DATA_ARTIFACT:
        os.system('mkdir -p artifacts tmp')
        os.system('rm -f artifacts/*')
    submitted = {}
    for p in pipeline.bridges.list(iterator=True, lazy=True):
        if p.stage != 'run tests':
            continue
        if not p.downstream_pipeline:
            continue
        pfull = project.pipelines.get(p.downstream_pipeline['id'])
        for j in pfull.jobs.list(iterator=True, lazy=True):
            j = project.jobs.get(j.id)
            if job_data == JOB_DATA_ARTIFACT:
                submitted.update(get_job_artifacts(j))
            else:
                submitted.update(get_job_trace(j))
    save_submitted(submitted, release)

    return submitted

def save_submitted(submitted, release):
        with open(f'submitted-{release}', 'w') as f:
            json.dump(submitted, f)

def load_submitted(release):
    with open(f'submitted-{release}') as f:
        submitted = json.load(f)
    return submitted

def print_submitted(submitted, verbose = VERBOSE_LOW):
    for k,v in submitted.items():
        if verbose == VERBOSE_LOW:
            print(k)
        elif verbose > VERBOSE_LOW:
            print(k, v)

def check_submitted(submitted, release, buildid, retry = RETRY_NONE, verbose = VERBOSE_LOW):
    if retry:
        gl = gitlab.Gitlab.from_config()
        gl.auth()
        project = gl.projects.get(config['gitlab_image_project'])
    valid = {}
    for d in ['apt', 'ostree']:
        tests = get_tests(release, buildid, d, verbose)
        valid.update(filter_tests(tests, SHOW_FAIL))
        valid.update(filter_tests(tests, SHOW_PASS))
    for s in submitted.keys():
        if not s in valid.keys():
            print(s, submitted[s])
            if retry == RETRY_JOB:
                job = project.jobs.get(submitted[s][0])
                print(f'Retrying job {job.id}')
                try:
                    job.retry()
                except Exception as ex:
                    print(f'Unable to retry {str(ex)}')
            elif retry == RETRY_LQA:
                print(f'Retrying LAVA job {s}')
                os.system(f'lqa resubmit {s}')

if __name__ == '__main__':

    action_choices = [
        'list',
        'check',
        'get-submitted',
        'list-submitted',
        'check-submitted',
    ]

    action_help = '''
    Executes action on Phabricator tasks:

        list: List tests in QA Report App
        check [id]: Check tests in QA Report App
        get-submitted: Get submitted tests from Gitlab CI
        list-submitted: List submitted tests previously saved
        check-submitted: Check submitted tests from Gitlab CI
    '''

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=action_help)
    parser.add_argument("-b","--buildid", default='latest', help="buildid")
    parser.add_argument("-d","--deployment", action='append', help="deployment")
    parser.add_argument("-g","--get", action='store_true', help="get information")
    parser.add_argument("-j","--job-data", type=int, default=JOB_DATA_TRACE, help="job data")
    parser.add_argument("-r","--release", action='append', help="release")
    parser.add_argument("-s","--show", type=int, default=SHOW_ALL, help="show")
    parser.add_argument("-v","--verbose", type=int, default=VERBOSE_LOW, help="verbose")
    parser.add_argument("-x","--retry", type=int, default=RETRY_NONE, help="retry")
    parser.add_argument("action", choices=action_choices, help='action to execute')

    args = parser.parse_args()

    read_config()

    if args.release == None:
        args.release = config['releases']

    if args.deployment == None:
        args.deployment = config['deployments']

    if args.action == 'list':
        for release in args.release:
            buildid = get_build(release, args.buildid)

            for dep in args.deployment:
                tests = get_tests(release, buildid, dep, args.verbose)
                tests = filter_tests(tests, args.show)
                print_tests(tests, args.verbose)
    if args.action == 'check':
        for release in args.release:
            buildid = get_build(release, args.buildid)

            for dep in args.deployment:
                tests = get_tests(release, buildid, dep, args.verbose)
                tests = filter_tests(tests, SHOW_INCOMPLETE)
                print_tests(tests, VERBOSE_NORMAL)
                if args.retry == RETRY_LQA:
                    retry_tests(tests)
    elif args.action == 'get-submitted':
        for release in args.release:
            buildid = get_build(release, args.buildid)
            submitted = get_submitted(release, buildid, args.job_data)
            print_submitted(submitted, args.verbose)
    elif args.action == 'list-submitted':
        for release in args.release:
            submitted = load_submitted(release)
            print_submitted(submitted, args.verbose)
    elif args.action == 'check-submitted':
        for release in args.release:
            buildid = get_build(release, args.buildid)
            if args.get:
                submitted = get_submitted(release, buildid, args.job_data)
            else:
                submitted = load_submitted(release)
            if not submitted:
                continue
            check_submitted(submitted, release, buildid, args.retry, args.verbose)
