#!/usr/bin/python3

import datetime
import fnmatch
import itertools
import os
import re
import shutil
import sys

# /srv/images/public/daily/17.12/20171213.0/arm64/development/apertis_17.12-development-arm64-uboot_20171213.0.img.gz

sysrootpattern = re.compile(r"sysroot-(?P<distribution>[a-z]+)-(?P<release>\d\d\.\d\d)-(?P<target>[a-z0-9]+)-(?P<build>\d{8}\.\d)\.tar\.gz")
sysrootconfpattern = re.compile(r"sysroot-(?P<distribution>[a-z]+)-(?P<release>\d\d\.\d\d)-(?P<target>[a-z0-9]+)$")

skip_items = [ "latest-images", "latest-scheduled-build" ]

# /srv/images/public/sysroot/17.12/sysroot-apertis-17.12-arm64-20171225.0.tar.gz
d = "/srv/images/internal"
d_public = "/srv/images/public"

def make_build_name(image):
    return os.path.join(
        image['periodic'],
        image['release'],
        image['build']
    )


def latest_release(directory):
    return sorted(fnmatch.filter(os.listdir(os.path.join(directory, 'daily')), '??.??'))[-1]


def check_orchestrator_build(image_path):
    orchestrator_metadata = "orchestrator.txt"
    orchestrator_metadata_path = os.path.join(image_path, "meta", orchestrator_metadata)

    build_orchestrator_pattern = re.compile(r"BUILD_ORIGIN=https://.*/infrastructure/builds-orchestrator/")

    if os.path.isfile(orchestrator_metadata_path):
        with open(orchestrator_metadata_path) as f:
            orch_metadata_content = f.read()

        build_orchestrator = build_orchestrator_pattern.search(orch_metadata_content)

        if build_orchestrator:
            return True
        return False
    return False


def scan_builds(directory, periodic='daily', release=None, filter_orchestrator=False):
    if release is None:
        release = latest_release(directory)
    today = datetime.datetime.today()
    if not os.access(os.path.join(directory, periodic, release), os.R_OK):
        return []
    for builddir in os.listdir(os.path.join(directory, periodic, release)):
        image = {'build': builddir, 'periodic': periodic, 'release': release}
        datestr = image['build'].partition('.')[0]
        if datestr in skip_items:
            continue
        if filter_orchestrator:
            image_path = os.path.join(directory, periodic, release, builddir)
            if not check_orchestrator_build(image_path):
                continue
        image['date'] = datetime.datetime.strptime(datestr, '%Y%m%d')
        image['weekday'] = image['date'].isoweekday()
        image['age'] = (today - image['date']).days
        image['testround'] = get_testround(image['date'])
        yield image

def get_testround(date):
    # test rounds happen on Wednesday, so images later than that are part of the next round
    (_, weeknumber, weekday) = date.isocalendar()
    return weeknumber + (0 if weekday <= 3 else 1)

def scan_sysroots(directory, release=None):
    if release is None:
        release = latest_release(directory)
    today = datetime.datetime.today()
    if not os.access(os.path.join(directory, "sysroot", release), os.R_OK):
        return []
    exclude = set()
    for sysrootconf in os.listdir(os.path.join(directory, "sysroot", release)):
        m = sysrootconfpattern.search(sysrootconf)
        if m:
            with open(os.path.join(directory, "sysroot", release, sysrootconf)) as f:
                for line in f:
                    if line.startswith('version='):
                        exclude.add(line.split(' ')[-1].strip())

    for sysroot in os.listdir(os.path.join(directory, "sysroot", release)):
        m = sysrootpattern.search(sysroot)
        if m:
            image = m.groupdict()
            if image['build'] in exclude:
                continue
            datestr = image['build'].partition('.')[0]
            image['date'] = datetime.datetime.strptime(datestr, '%Y%m%d')
            image['weekday'] = image['date'].isoweekday()
            image['age'] = (today - image['date']).days
            image['file'] = sysroot
            yield image

def link_weeklies(directory, release):
    today = datetime.datetime.today()
    sort_builds = lambda b: (b['testround'], b['build'])
    builds = sorted(scan_builds(directory, 'daily', release, filter_orchestrator=True), key=sort_builds)
    testrounds = itertools.groupby(builds, key=lambda b: b['testround'])
    for key, testround in testrounds:
        # skip the current testround, we still need to generate the final image for it
        if key == get_testround(today):
            continue
        testround = list(testround)
        # pick the most recent build in the round
        image = testround[-1]
        src_dir = os.path.join(directory, 'daily', image['release'], image['build'])
        dst_dir = os.path.join(directory, 'weekly', image['release'], image['build'])
        if not os.path.exists(dst_dir):
            print('weekly: ' + src_dir)
            shutil.copytree(src_dir, dst_dir, copy_function=os.link)

def remove_old_dailies(directory, release):
     for image in scan_builds(directory, 'daily', release):
        if image['age'] > 7:
            src_dir = make_build_name(image)
            print('rmdir: ' + os.path.join(directory, src_dir))
            shutil.rmtree(os.path.join(directory, src_dir), True)

     for image in scan_builds(directory, 'lxc', release):
        if image['age'] > 7:
            src_dir = make_build_name(image)
            print('rmdir: ' + os.path.join(directory, src_dir))
            shutil.rmtree(os.path.join(directory, src_dir), True)

     for image in scan_sysroots(directory, release):
        if image['age'] > 7:
            print('rm: ' + os.path.join(directory, "sysroot", image['release'], image['file']))
            os.remove(os.path.join(directory, "sysroot", image['release'], image['file']))

def remove_old_weeklies(directory, release):
     for image in scan_builds(directory, 'weekly', release):
        if image['age'] > 90:
            src_dir = make_build_name(image)
            print('rmdir: ' + os.path.join(directory, src_dir))
            shutil.rmtree(os.path.join(directory, src_dir), True)


distro = None
if len(sys.argv) == 2:
    distro = sys.argv[1]

link_weeklies(d, distro)
remove_old_dailies(d, distro)
remove_old_weeklies(d, distro)
link_weeklies(d_public, distro)
remove_old_dailies(d_public, distro)
remove_old_weeklies(d_public, distro)
