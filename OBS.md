# Setting up new repository on OBS and related services

This document outlines the steps required to add a new repository to the OBS service, as well as other dependent services like `reprepro` and the APT repository frontend.


## Steps to create a new OBS repository

* Create new repository on the OBS server using the below example `osc` command
  * Locally create a template file with the repository information:
    ```
    <project name="apertis:demo:rpi64-containers">
    <title>apertis:demo:rpi64-containers</title>
    <description>Repository for Apertis Demo custom images.&#13;
	https://gitlab.apertis.org/demo/image-recipes-rpi64-containers/</description>
    <person userid="Admin" role="maintainer"/>
    <person userid="adalessandro" role="maintainer"/>
    <person userid="andrewsh" role="maintainer"/>
    <person userid="apertis-gitlab" role="maintainer"/>
    <person userid="em" role="maintainer"/>
    <person userid="ritesh" role="maintainer"/>
    <group groupid="apertis-dev" role="maintainer"/>
    <repository name="default" rebuild="local" block="never">
	<path project="apertis:v2021:development" repository="default"/>
	<path project="apertis:v2021:target" repository="default"/>
	<arch>aarch64</arch>
    </repository>
    </project>
    ```
  * Run (as `Admin`) the `osc meta prj` command to create the repository using the local template file created in the previous step:
    ```
    osc meta prj apertis:demo:rpi64-containers -F "$TEMPLATE_FILE"
    ```
  * The OBS Service could be running on `bare-metal` or a `containerized` environment. The user is expected to adapt the commands based on the environment that OBS is run in.
    For example, if running in a containerized environment, please ascertain the same using the following commands:
    ```
    docker ps
    ```
    The above command will provide the name of the running container.

    ```
    docker exec -it name_of_container bash
    ```
    The above command will spawn the `bash` shell inside the running container. Thereafter, the user can execute the below commands in the OBS Backend environment.

  * Create the necessary directories on the OBS backend server:
    ```
    mkdir -p /srv/obs/build/apertis:demo:rpi64-containers/default
    chown obsrun:obsrun -R /srv/obs/build/apertis:demo:rpi64-containers
    ```
  * Add the new repository details into `/etc/obs/BSConfig.local.pm`:
    ```
    "apertis:demo:rpi64-containers/default" =>
    { "repository" => "shared/apertis/public/rpi64-containers",
      "codename" => "demo",
      "component" => "rpi64-containers" },
    ```
  * Export the newly created repository:
    ```
    $ sudo -u obsrun reprepro --gnupghome /srv/obs/gnupg/ -Vb /srv/obs/repos/shared/apertis/public/rpi64-containers/ export demo
    Exporting demo...
    Created directory "/srv/obs/repos/shared/apertis/public/rpi64-containers//dists/demo/rpi64-containers"
    Created directory "/srv/obs/repos/shared/apertis/public/rpi64-containers//dists/demo/rpi64-containers/binary-arm64"
    Created directory "/srv/obs/repos/shared/apertis/public/rpi64-containers//dists/demo/rpi64-containers/source"
     generating rpi64-containers/Contents-arm64...
    Successfully created '/srv/obs/repos/shared/apertis/public/rpi64-containers//dists/demo/Release.gpg.new'
    Successfully created '/srv/obs/repos/shared/apertis/public/rpi64-containers//dists/demo/InRelease.new'
    ``` 
  * Restart the OBS Scheduler service

    ```
    supervisorctl restart scheduler@aarch64 scheduler@armv7hl scheduler@x86_64
    ```
  * Restart the OBS Publisher service

    ```
    supervisorctl restart obspublisher
    ```
  * Republish the newly created OBS repository:
    ```
    obs_admin --republish-repository apertis:demo:rpi64-containers default
    ```

* Validate the repository and its attributes on the OBS backend host:

    ```
    root@niobium:/srv/obs/repos/shared/apertis/public/rpi64-containers# ls
    conf  db  dists  lists  logs  pool
    ```

  * Validate the repository configuration is inline with the created repository:

    ```
    $ cat conf/distributions
    Origin: Apertis
    Label: Apertis Demo v2021 repository
    Codename: v2021
    AlsoAcceptFor: unstable testing quantal raring trusty experimental
    Architectures: amd64 arm64 source
    Components: rpi64-containers
    Description: Apertis demo repository for v2021
    SignWith: EB5691FC
    ValidFor: 18m
    Contents: .bz2
    DebIndices: Packages Release . .gz
    Tracking: all includechanges keepsources
    Log: reprepro-demo-v2021.log
    ```

  * Validate the repository logs has information logged about packages added:

    ```
    root@niobium:/srv/obs/repos/shared/apertis/public/rpi64-containers# head logs/reprepro-demo-v2021.log
    2021-08-30 16:41:20 add v2021 dsc rpi64-containers source arm-trusted-firmware 2.5+dfsg-1+apertis1
    2021-08-30 16:41:35 add v2021 deb rpi64-containers arm64 arm-trusted-firmware 2.5+dfsg-1+apertis1bv2021.0b2
    2021-08-30 16:41:35 add v2021 deb rpi64-containers amd64 arm-trusted-firmware-tools 2.5+dfsg-1+apertis1bv2021.0b1
    2021-08-30 16:41:35 add v2021 deb rpi64-containers arm64 arm-trusted-firmware-tools 2.5+dfsg-1+apertis1bv2021.0b2
    2021-08-30 16:41:35 add v2021 deb rpi64-containers amd64 arm-trusted-firmware-tools-dbgsym 2.5+dfsg-1+apertis1bv2021.0b1
    2021-08-30 16:41:35 add v2021 deb rpi64-containers arm64 arm-trusted-firmware-tools-dbgsym 2.5+dfsg-1+apertis1bv2021.0b2
    2021-08-30 16:44:03 add v2021 dsc rpi64-containers source cached-property 1.5.1-3
    2021-08-30 16:44:03 add v2021 deb rpi64-containers amd64 python3-cached-property 1.5.1-3bv2021.0b1
    2021-08-30 16:44:03 add v2021 deb rpi64-containers arm64 python3-cached-property 1.5.1-3bv2021.0b1
    2021-08-30 16:44:03 add v2021 deb rpi64-containers amd64 python-cached-property 1.5.1-3bv2021.0b1
    ```

  * Validate the apt metadata is generated:

    ```
    root@niobium:/srv/obs/repos/shared/apertis/public/rpi64-containers# ls -lh dists/v2021/
    total 20K
    lrwxrwxrwx 1 obsrun obsrun   35 Sep  1 16:34 Contents-amd64.bz2 -> rpi64-containers/Contents-amd64.bz2
    lrwxrwxrwx 1 obsrun obsrun   35 Sep  1 16:34 Contents-arm64.bz2 -> rpi64-containers/Contents-arm64.bz2
    lrwxrwxrwx 1 obsrun obsrun   35 Aug 30 16:07 Contents-armhf.bz2 -> rpi64-containers/Contents-armhf.bz2
    -rw-r--r-- 1 obsrun obsrun 4.6K Sep  1 16:34 InRelease
    -rw-r--r-- 1 obsrun obsrun 3.7K Sep  1 16:34 Release
    -rw-r--r-- 1 obsrun obsrun  833 Sep  1 16:34 Release.gpg
    drwxr-xr-x 6 obsrun obsrun 4.0K Sep  1 16:34 rpi64-containers
    ```

  * Validate that the repository is populated with debian packages:

    ```
    root@niobium:/srv/obs/repos/shared/apertis/public/rpi64-containers# tree pool/
    pool/
    └── rpi64-containers
	├── a
	│   └── arm-trusted-firmware
	│       ├── arm-trusted-firmware_2.5+dfsg-1+apertis1bv2021.0b1_amd64.changes
	│       ├── arm-trusted-firmware_2.5+dfsg-1+apertis1bv2021.0b2_arm64.changes
	│       ├── arm-trusted-firmware_2.5+dfsg-1+apertis1bv2021.0b2_arm64.deb
	│       ├── arm-trusted-firmware_2.5+dfsg-1+apertis1.debian.tar.xz
	│       ├── arm-trusted-firmware_2.5+dfsg-1+apertis1.dsc
	│       ├── arm-trusted-firmware_2.5+dfsg-1+apertis2bv2021.0b3_amd64.changes
    ```

* Ensure that the new repository shows up on the APT Frontend repository (`Host: rodoric`)
  With the repository properly set up on the OBS Backend, it will momentarily be synchronized to the frontend host at the following example path

    ```
    /srv/repo/repositories/apertis/public/rpi64-containers
    ```

* Ensure proper directory and symlink are setup in the Apache webroot directory

    ```
    # ls -lh /srv/repositories.apertis.org/www/rpi64-containers/
    total 4.0K
    lrwxrwxrwx 1 root root 60 Aug 26 16:40 dists -> /srv/repo/repositories/apertis/public/rpi64-containers/dists
    lrwxrwxrwx 1 root root 59 Aug 26 16:40 pool -> /srv/repo/repositories/apertis/public/rpi64-containers/pool
    ```

* The APT repository should be available at the following location:

    ```
    https://repositories.apertis.org/rpi64-containers/dists/demo/rpi64-containers/binary-arm64/
    ```

* The APT repository can be validated with any Apertis client with the following entry in its `sources.list`:

    ```
    deb https://repositories.apertis.org/rpi64-containers demo rpi64-containers
    ```
